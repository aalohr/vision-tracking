#ifndef __IMAGE_SEQUENCE_INPUT_H__
#define __IMAGE_SEQUENCE_INPUT_H__

#include "opencv2/nonfree/nonfree.hpp"
#include "dirent.h"
using namespace std;

class ImageSequenceInput_C 
{
public:
	ImageSequenceInput_C() : m_valid(false){};

	virtual cv::Mat getFrame() = 0;
	bool isValid(){return m_valid;};

protected:
	void setValid(bool valid){m_valid = valid;};
private:
	
	bool m_valid;
};

class VideoImageSequenceInput_C : public ImageSequenceInput_C
{
public:
	VideoImageSequenceInput_C(std::string video_path);

	virtual cv::Mat getFrame();
	
private:
	cv::VideoCapture m_videoCapture;
};

class SeriesImageSequenceInput_C : public ImageSequenceInput_C
{
public:
	SeriesImageSequenceInput_C(std::string sequence_folder);

	virtual cv::Mat getFrame();
	
private:
	std::vector<cv::Mat> m_images;
	std::vector<cv::Mat>::iterator m_currentImage;
};

SeriesImageSequenceInput_C::SeriesImageSequenceInput_C(std::string sequence_folder)
	:ImageSequenceInput_C()
{
	//Read files from folder
	DIR* dir;
	struct dirent *ent;
	std::stringstream ent_name;
	if (!(dir=opendir(sequence_folder.c_str()))) { std::cout << "Wrong folder name" << std::endl; setValid(false);};

	while (((ent = readdir(dir)) != 0))
	{
		ent_name.str("");	//Clear the image path;
		ent_name << sequence_folder << "\\" << ent->d_name;
		std::cout << "Attemping to load " << ent_name.str() << std::endl;
		cv::Mat image = cv::imread(ent_name.str(), CV_LOAD_IMAGE_COLOR);
		
		if (image.data) 
		{
			m_images.push_back(image);
		};


	}

	if (m_images.size() > 0)
	{
		setValid(true);
		m_currentImage = m_images.begin();
	}
	else
	{
		std::cout << "Could not find any images in the given folder! " << std::endl;
	}

	closedir(dir);
};

cv::Mat SeriesImageSequenceInput_C::getFrame()
{
	m_currentImage++;

	if (m_currentImage == m_images.end())
	{
		return cv::Mat();
	}

	return (*m_currentImage);
}

VideoImageSequenceInput_C::VideoImageSequenceInput_C(std::string video_path)
	:ImageSequenceInput_C()
{
	 m_videoCapture.open(video_path);

    if (!m_videoCapture.isOpened())
    {
        cout << "capture device " << video_path<< " failed to open!" << endl;
		setValid(false);	
    }
	else
	{
		cout << "Succesfully opened " << video_path << std::endl;
		setValid(true);
	}
}

cv::Mat VideoImageSequenceInput_C::getFrame()
{
	cv::Mat Frame;
	m_videoCapture >> Frame;
	return Frame;
}

#endif