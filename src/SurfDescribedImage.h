#ifndef __SURF_DESCRIBED_IMAGE_H__
#define __SURF_DESCRIBED_IMAGE_H__

#include "opencv2/nonfree/features2d.hpp"

#include "DescribedImage.h"

class SurfDescribedImage_C : public DescribedImage_C
{
public:

	SurfDescribedImage_C(cv::Mat image);

	cv::Mat GetImage() {return m_image;};
	cv::Mat GetDescriptors() {return m_descriptors;};
	std::vector<cv::KeyPoint> GetKeypoints(){return m_keypoints;};

private:
	virtual void calculateDescriptors();

	cv::Mat m_image;
	cv::Mat m_descriptors;
	std::vector<cv::KeyPoint> m_keypoints;
};

void SurfDescribedImage_C::calculateDescriptors()
{
	cv::SurfFeatureDetector surfer(500);
	surfer.detect(m_image, m_keypoints);

	cv::SurfDescriptorExtractor extractor(500);
	extractor.compute(m_image, m_keypoints, m_descriptors);

};

SurfDescribedImage_C::SurfDescribedImage_C(cv::Mat image) : m_image(image)
{
	calculateDescriptors();
};


#endif //__SURF_DESCRIBED_IMAGE_H__