#ifndef __Sift_DESCRIBED_IMAGE_H__
#define __Sift_DESCRIBED_IMAGE_H__

#include "opencv2/nonfree/features2d.hpp"

#include "DescribedImage.h"

class SiftDescribedImage_C : public DescribedImage_C
{
public:

	SiftDescribedImage_C(cv::Mat image);

	cv::Mat GetImage() {return m_image;};
	cv::Mat GetDescriptors() {return m_descriptors;};
	std::vector<cv::KeyPoint> GetKeypoints(){return m_keypoints;};

private:
	virtual void calculateDescriptors();

	cv::Mat m_image;
	cv::Mat m_descriptors;
	std::vector<cv::KeyPoint> m_keypoints;
};

void SiftDescribedImage_C::calculateDescriptors()
{
	cv::SiftFeatureDetector Sifter(400);
	Sifter.detect(m_image, m_keypoints);

	cv::SiftDescriptorExtractor extractor(400);
	extractor.compute(m_image, m_keypoints, m_descriptors);

};

SiftDescribedImage_C::SiftDescribedImage_C(cv::Mat image) : m_image(image)
{
	calculateDescriptors();
};


#endif //__Sift_DESCRIBED_IMAGE_H__