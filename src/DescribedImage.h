#ifndef __DESCRIBED_IMAGE_H__
#define __DESCRIBED_IMAGE_H__

#include "opencv2/features2d/features2d.hpp"

#include <iostream>

class DescribedImage_C
{
public:

	DescribedImage_C(){};
	DescribedImage_C(cv::Mat image);

	virtual cv::Mat GetImage() {return m_image;};
	virtual cv::Mat GetDescriptors() {return m_descriptors;};
	virtual std::vector<cv::KeyPoint> GetKeypoints(){return m_keypoints;};
	virtual std::vector<cv::Point2f> GetCorners(cv::Mat H);

private:
	virtual void calculateDescriptors(){std::cout << "Calculating Descriptors has not yet been implemented for this image " <<std::endl;};

	cv::Mat m_image;
	cv::Mat m_descriptors;
	std::vector<cv::KeyPoint> m_keypoints;
};

DescribedImage_C::DescribedImage_C(cv::Mat image) : m_image(image)
{
};

//Transforms the corners using the specified perspective transform H
std::vector<cv::Point2f> DescribedImage_C::GetCorners(cv::Mat H)
{
				
	//-- Get the corners from the image_1 ( the object to be "detected" )
	std::vector<cv::Point2f> obj_corners(4);
	obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( GetImage().cols, 0 );
	obj_corners[2] = cvPoint( GetImage().cols, GetImage().rows ); obj_corners[3] = cvPoint( 0, GetImage().rows );
	std::vector<cv::Point2f> scene_corners(4);

	cv::perspectiveTransform( obj_corners, scene_corners, H);

	return scene_corners;
}

#endif //__DESCRIBED_IMAGE_H__