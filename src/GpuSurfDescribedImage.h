#ifndef __GPU_SURF_DESCRIBED_IMAGE_H__
#define __GPU_SURF_DESCRIBED_IMAGE_H__

#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/nonfree/gpu.hpp"

#include "DescribedImage.h"

class GpuSurfDescribedImage_C : public DescribedImage_C
{
public:

	GpuSurfDescribedImage_C(cv::Mat image);

	cv::Mat GetImage() {return m_image;};
	cv::Mat GetDescriptors() {return m_descriptors;};
	std::vector<cv::KeyPoint> GetKeypoints(){return m_keypoints;};

private:
	virtual void calculateDescriptors();

	cv::Mat m_image;
	cv::Mat m_descriptors;
	std::vector<cv::KeyPoint> m_keypoints;
};

void GpuSurfDescribedImage_C::calculateDescriptors()
{
	cv::gpu::GpuMat gpu_image;
	cv::gpu::GpuMat keypoints_gpu, descriptors_gpu;

	gpu_image.upload(m_image);
	cv::gpu::SURF_GPU surf;
	surf(gpu_image, cv::gpu::GpuMat(), keypoints_gpu, descriptors_gpu, false);

	std::vector<float> descriptors_cpu;
	surf.downloadDescriptors(descriptors_gpu, descriptors_cpu);

	m_descriptors = cv::Mat(descriptors_cpu);

	surf.downloadKeypoints(keypoints_gpu, m_keypoints);

};

GpuSurfDescribedImage_C::GpuSurfDescribedImage_C(cv::Mat image) : m_image(image)
{
	calculateDescriptors();
};


#endif //__GPU_SURF_DESCRIBED_IMAGE_H__