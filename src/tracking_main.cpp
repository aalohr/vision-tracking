/*
 * A Demo to OpenCV Implementation of SURF
 * Further Information Refer to "SURF: Speed-Up Robust Feature"
 * Author: Liu Liu
 * liuliu.1987+opencv@gmail.com
 */
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/legacy/legacy.hpp"
#include "opencv2/legacy/compat.hpp"


#include <iostream>
#include <vector>
#include <stdio.h>

#include "SurfDescribedImage.h"
#include "SiftDescribedImage.h"
#include "GpuSurfDescribedImage.h"
#include "ImageSequenceInput.h"

using namespace std;
using namespace cv;

#define USE_GPU 0	//No cuda support!

#if (USE_GPU)
typedef GpuSurfDescribedImage_C TrackingDescribedImage_C ;	//Using SURF at the moment, but can be changed
#else
typedef SurfDescribedImage_C TrackingDescribedImage_C ;	//Using SURF at the moment, but can be changed
#endif

static const double BAD_MATCH_RATIO = 0.8;
static const double GOOD_MATCH_RATIO = 0.1;
static const int MIN_GOOD_MATCHES = 8;

static const double MAX_AREA_CHANGE = 0.5;
static const double MIN_AREA = 300;

static const int MAX_DR_FRAMES_TRUSTED = 4;
static const int MAX_DR_FRAMES = 10;
static const double ABS_MIN_DIST = 0.05;

//Taken from sample!
static CvScalar colors[] =
{
    {{0,0,255}},
    {{0,128,255}},
    {{0,255,255}},
    {{0,255,0}},
    {{255,128,0}},
    {{255,255,0}},
    {{255,0,0}},
    {{255,0,255}},
    {{255,255,255}}
};

typedef struct DescribedFeature 
{
	DescribedImage_C* described_image;
	std::vector<cv::Point2f> previousCorners;
	std::vector<cv::Point2f> currentCorners;
	int framesSinceObservation;
} DescribedFeature_T;

bool processCommandLine(int argc, char** argv, ImageSequenceInput_C* &sequence, std::vector<cv::Mat> &features)
{
	
	const cv::String keys = 
		"{ v| video |   | video to track within}"
		"{ i| images |  | image  sequence folder}"
		"{ a| featurea |   | 1st feature to look for}"
		"{ b| featureb |   | 2nd feature to look for}"
		"{ c| featurec |   | 3rd feature to look for}"
		;

	std::vector<std::string> feature_ids;
	feature_ids.push_back("featurea"); feature_ids.push_back("featureb"); feature_ids.push_back("featurec");

	CommandLineParser parser(argc, argv, keys.c_str());

	//Open the video input given from command line
	cv::String video_address = parser.get<cv::String>("video");
	cv::String image_series_folder = parser.get<cv::String>("images");

	if (video_address != "")
	{
		sequence = new VideoImageSequenceInput_C(video_address);
	}
	else if (image_series_folder != "")
	{
		sequence = new SeriesImageSequenceInput_C(image_series_folder);
	}

	//Get the features to be found in each video.
	for (int i = 0; i < 3; i++)
	{
		std::string arg_id( feature_ids[i]);
		cv::String feature_address = parser.get<String>(arg_id);
		std::cout << "feature " << arg_id <<" address : " << feature_address << std::endl;
		cv::Mat feature = imread(feature_address, CV_LOAD_IMAGE_GRAYSCALE);
		
		//If loaded correctly, push back!
		if (!!feature.data)
		{
			features.push_back(feature);
		}
	}

	if (features.size() == 0)
	{
		cout << "No Features Specified! Specify -1 -2 -3 for features to track" << std::endl;
		return false;
	}

	return true;
}

bool getHomography(DescribedImage_C* frame, DescribedImage_C* feature, cv::Mat &H)
{
	FlannBasedMatcher matcher;
	std::vector<DMatch > matches;

	matcher.match(feature->GetDescriptors(),  frame->GetDescriptors(), matches);
			
	std::vector< DMatch > good_matches;

	double max_dist = 0; double min_dist = 100;

	for( int i = 0; i < feature->GetDescriptors().rows; i++ )
	{ double dist = matches[i].distance;
		if( dist < min_dist ) min_dist = dist;
		if( dist > max_dist ) max_dist = dist;
	}

	if (min_dist < ABS_MIN_DIST) min_dist = ABS_MIN_DIST;

	for( int i = 0; i < feature->GetDescriptors().rows; i++ )
	{ if( matches[i].distance <= 3*(min_dist) )
			{ good_matches.push_back( matches[i]); }
	}

	std::vector<Point2f> obj;
	std::vector<Point2f> scene;
				
	for( int i = 0; i < good_matches.size(); i++ )
	{
	//-- Get the keypoints from the good matches
	obj.push_back( feature->GetKeypoints()[ good_matches[i].queryIdx ].pt );
	scene.push_back( frame->GetKeypoints()[ good_matches[i].trainIdx ].pt );
	}

	std::cout << " good matches: " << good_matches.size() << " - min dist: " << min_dist << std::endl;
	std::cout << " feature keypoints " << feature->GetKeypoints().size() << std::endl;
				
	if (good_matches.size() >= BAD_MATCH_RATIO*feature->GetKeypoints().size() || good_matches.size() < MIN_GOOD_MATCHES)
	{
		//too many 'good' matches, based on our heuristic this indicates minimum dist is bad!
		return false;
	}

	H = findHomography( obj, scene, CV_RANSAC );

	return true;

}

Point2f getCentroid(std::vector<Point2f> points)
{
	Point2f c(0,0);

	for (int i=0; i<4; i++)
	{
		c += 0.25 * points[i];
	}

	return c;
}

//Calculate the area of a quadrilateral
double areaOfQuad(std::vector<Point2f> points)
{
	Point2f A = points[0]; Point2f B = points[1]; Point2f C = points[2];Point2f D = points[3];

	double areaTriangle1 = abs((A.x * (B.y-D.y) + B.x*(D.y-A.y) + D.x*(A.y-B.y)) / 2);
	double areaTriangle2 = abs((B.x * (D.y-C.y) + D.x*(C.y-B.y) + C.x*(B.y-D.y)) / 2);

	return areaTriangle1+areaTriangle2;
}

bool checkCornerOrder(std::vector<Point2f> points)
{
	if (points[0].x < points[1].x && points[2].x < points[3].x) return false;
	if (points[0].x > points[1].x && points[2].x > points[3].x) return false;
	if (points[0].y < points[3].y && points[1].y > points[2].y) return false;
	if (points[0].y > points[3].y && points[1].y < points[2].y) return false;
	return true;
}

//If any distance between corners over this size
bool bigCorners(std::vector<Point2f> points, double maxSize)
{
	for (int i=0; i<4; i++)
	{
		if ((points[i%4]-points[(i+1)%4]).dot((points[i%4]-points[(i+1)%4])) > maxSize*maxSize)
		{
			return true;
		}
	}
	return false;
}

bool validateNewCorners(DescribedFeature_T* described_feature, std::vector<Point2f> newCorners)
{

	std::cout << "since last obs: " << described_feature->framesSinceObservation << std::endl;

	double newArea = areaOfQuad(newCorners);
			std::cout << "Area : "<<newArea << std::endl;
	if (newArea < MIN_AREA)
	{
		std::cout << "area too small!" << std::endl;
		//this can't possibly be correct transform
		return false;
	}

	if (!checkCornerOrder(newCorners))
	{
		std::cout << "corners out of order!" << std::endl;
		return false;
	}

	if (bigCorners(newCorners, 3*max(described_feature->described_image->GetImage().cols, described_feature->described_image->GetImage().rows)))
	{
		std::cout << "corners too big! " << std::endl;
		return false;
	}

	if (described_feature->currentCorners.size() == 4)
	{
	
		//check the area has not varied wildly
		double currentArea = areaOfQuad(described_feature->currentCorners);

		if (newArea < MAX_AREA_CHANGE * currentArea || newArea > currentArea / MAX_AREA_CHANGE)
		{			
			std::cout << "area changed too much" << std::endl;
			return false;
		}
					
		//Check that the object has not moved too far (not counting too much dead reckoning!)
		if (!(described_feature->framesSinceObservation >= MAX_DR_FRAMES_TRUSTED))
		{
			
			double dist2 = 0;

			for (int i=0; i<4; i++)
			{
				dist2 += 1 * (described_feature->currentCorners[i] - newCorners[i]).dot(described_feature->currentCorners[i] - newCorners[i]);
			}

			if (dist2 > 1 * (described_feature->currentCorners[0] -described_feature->currentCorners[1]).dot(described_feature->currentCorners[0] -described_feature->currentCorners[1]))
			{
				std::cout << "centroid moved too far " << dist2 << std::endl;
				//return false;
			}
		}
	}

	return true;
}

int main(int argc, char** argv)
{

	std::vector<cv::Mat> features;
	ImageSequenceInput_C* sequence = 0;

	if (!processCommandLine(argc, argv, sequence, features))
	{
		return 1;
	}

	if (!sequence || !sequence->isValid())
	{
		std::cout << "No image capture defined!" <<std::endl;
		return 1;
	}

	Mat frame;

	std::vector<DescribedFeature_T*> described_features;

	for (std::vector<cv::Mat>::iterator i = features.begin(); i != features.end(); i++)
	{
		DescribedFeature_T* f = new DescribedFeature_T(); 
		f->described_image = new TrackingDescribedImage_C(*i);
		described_features.push_back(f);
	}

	DescribedImage_C* described_frame = 0;

    for (;;)
    {
		if (described_frame != 0) 
		{
			delete described_frame;
		}

		//Grab frame from the video
        frame = sequence->getFrame();

        if (frame.empty())
            break;

		//convert to grayscale for SURF
		Mat frame_gray;
		cvtColor(frame, frame_gray, COLOR_BGR2GRAY);

		//Assign simple pointer for finding the frame
		described_frame = new TrackingDescribedImage_C(frame_gray);

		for (std::vector<DescribedFeature_T*>::iterator f = described_features.begin(); f!= described_features.end(); f++)
		{
			//Assign simple pointer for the feature
			DescribedFeature_T* described_feature = (*f);

			cv::Mat H;
			cv::Scalar colour;

			bool draw = false;
			bool dead_reckon = false;


			//Try to get the transformation between descriptors.
			if (!getHomography(described_frame, described_feature->described_image, H))
			{
				dead_reckon = true;
				colour = Scalar(0, 0, 255);
			}
			else
			{
				//Holometry is found - make sure it's valid!
				colour = Scalar(0, 255, 255);
				std::vector<Point2f> newCorners = described_feature->described_image->GetCorners(H);
				dead_reckon = !validateNewCorners(described_feature, newCorners);

				//Hopefully the transform is good! no need to fudge it!
				if (!dead_reckon)
				{
					described_feature->previousCorners = described_feature->currentCorners;
					described_feature->currentCorners = newCorners;
					colour = Scalar(0, 255, 0);
					draw = true;
					described_feature->framesSinceObservation = 0;
				}
			}

			if (dead_reckon)
			{
				if (described_feature->previousCorners.size() == 4 && described_feature->currentCorners.size() == 4)
				{
					std::vector<Point2f> deadReckoned;
					Point2f centroidDiff = getCentroid(described_feature->currentCorners) - getCentroid( described_feature->previousCorners);
	
					for (int i=0; i < 4; i++) 
					{deadReckoned.push_back(described_feature->currentCorners[i] - centroidDiff);};

					//Move the corners (for estimation later)
					described_feature->previousCorners = described_feature->currentCorners;
					described_feature->currentCorners = deadReckoned;
					
					draw = true;
				}
				else if (described_feature->currentCorners.size() == 4)
				{
					described_feature->previousCorners = described_feature->currentCorners;
					
					draw = true;
				}

				if (described_feature->framesSinceObservation >= MAX_DR_FRAMES)
				{
					described_feature->previousCorners.clear();
					described_feature->currentCorners.clear();
				}

				described_feature->framesSinceObservation++;
			}

			//-- Draw lines between the corners (the mapped object in the scene - image_2 )
			if (draw) 
			{
				for (int i=0; i<4; i++)
				{
					line( frame, described_feature->currentCorners[i] , described_feature->currentCorners[(i+1)%4], colour, 3 );
				}
			}
		}

		imshow("frame", frame);

		waitKey(1);
	}
	delete(sequence);
	//memory leak, but who cares?
	while(!described_features.empty()) delete described_features.back()->described_image, delete described_features.back(), described_features.pop_back();

	exit(0);
    return 0;
}
